﻿using Xamarin.Forms;
using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace APIapp
{
	public partial class APIappPage : ContentPage
	{
		List<DPM_news> lstDpm = new List<DPM_news>();
		List<DPM_newList> lstDpmNewsList = new List<DPM_newList>();
		public APIappPage()
		{
			InitializeComponent();
			//Task<string> ts = this.CallAPIGet();

			//Task s = this.GetTitleData();
			Task NewsList = this.GetDetail("");
			//lstData.ItemsSource = lstDpmNewsList;
		}
		private async Task GetDetail(string id)
		{

			var httpClient = new HttpClient();
			var response = await httpClient.GetAsync("http://dpm.depth1st.com/portal/mobile?cmd=listNews&pageSize=20&position=0&stats=1&status=2");
			var body = await response.Content.ReadAsStringAsync();

			var json = JObject.Parse(body);

			var row = json["newsList"];

			lstDpmNewsList = JsonConvert.DeserializeObject<List<DPM_newList>>(row.ToString());

			lstData.ItemsSource = lstDpmNewsList;
		}

		void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
		{
			
		}
		public async Task GetTitleData()
		{
			
			var client = new HttpClient();
			var item = await client.GetAsync("http://dpm.depth1st.com/portal/mobilews?queryCode=ACD001&STARTDATE=2017-01-01&ENDDATE=2017-01-01");
			var text = await item.Content.ReadAsStringAsync();

			var json = JObject.Parse(text);

			var row = json["rows"];

			lstDpm = JsonConvert.DeserializeObject<List<DPM_news>>(row.ToString());

			//lblDisplay.Text = "DPM REPORTER";


		}


	}
}
