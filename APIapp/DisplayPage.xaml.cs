﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;

using Xamarin.Forms;

namespace APIapp
{
	public partial class DisplayPage : ContentPage
	{
		//List<DPM_news> lstDpm = new List<DPM_news>();
		List<DPM_newList> lstDpmNewsList = new List<DPM_newList>();

		public DisplayPage()
		{
			//NewsImage.Source = "http://www.swpark.or.th/components/com_jumi/files/tenant/Company_Logo/21062012124225logo_df_%E0%B9%82%E0%B8%9B%E0%B8%A3%E0%B9%88%E0%B8%87.png";

			InitializeComponent();

			Task ts = this.GetDetailAll();


		}
		// Caching Image
		public void SetObject()
		{
			//var webImage = new Image { Aspect = Aspect.AspectFit };
			//webImage.Source = new UriImageSource
			//{
			//	Uri = new Uri(""),
			//	CachingEnabled = true,
			//	CacheValidity = new TimeSpan(0, 0, 1, 0)//new TimeSpan(1, 0, 0, 0)
			//};
			//NewsImage = webImage;
			//DisplayAlert("Cache", "SetObject", "OK");
		}
		public void SaveCache()
		{
			

		}

		void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
		{

			//int index = (Int32)e.SelectedItem;

			DPM_newList item = (DPM_newList)e.SelectedItem;
			Navigation.PushAsync(new DetailPage(item.ID));//param is ID

		}

		private async Task GetDetailAll()
		{
			var httpClient = new HttpClient();
			var response = await httpClient.GetAsync("http://dpm.depth1st.com/portal/mobile?cmd=listNews&pageSize=20&position=0&stats=1&status=2");
			var body = await response.Content.ReadAsStringAsync();

			var json = JObject.Parse(body);

			var row = json["newsList"];

			lstDpmNewsList = JsonConvert.DeserializeObject<List<DPM_newList>>(row.ToString());
			for (int i = 0; i < lstDpmNewsList.Count; i++)
			{
				lstDpmNewsList[i].lastestImage = "http://dpm.depth1st.com/portal/download/" + lstDpmNewsList[i].lastestImage;
				lstDpmNewsList[i].DETAIL = lstDpmNewsList[i].DETAIL.Substring(0, 80).Trim() + "...";
			}
			listDisplayData.ItemsSource = lstDpmNewsList;
		}
	}
}
